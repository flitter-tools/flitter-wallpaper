#ifndef FOLDERITEM_H
#define FOLDERITEM_H

#include <QString>
#include <QIcon>

class FolderItem
{
    Q_DISABLE_COPY(FolderItem)

    public:
        FolderItem(const QString &path, const QString &icon);
        ~FolderItem();

        QIcon icon() const;
        QString path() const;
        QString description() const;

    private:
        QString m_path;
        QString m_description;
        QIcon m_icon;
};

#endif // FOLDERITEM_H
