#include "x11.h"

// Adapted from fluxbox' bsetroot
void set_root_atoms(Display *display, Pixmap pixmap)
{
    int screen = 0;

    Atom atom_root, atom_eroot, type;
    unsigned char *data_root, *data_eroot;
    int format;
    unsigned long length, after;

    atom_root = XInternAtom(display, "_XROOTMAP_ID", True);
    atom_eroot = XInternAtom(display, "ESETROOT_PMAP_ID", True);

    // doing this to clean up after old background
    if (atom_root != None && atom_eroot != None)
    {
        XGetWindowProperty(display, RootWindow(display, screen), atom_root, 0L, 1L, False, AnyPropertyType, &type, &format, &length, &after, &data_root);

        if (type == XA_PIXMAP)
        {
            XGetWindowProperty(display, RootWindow(display, screen), atom_eroot, 0L, 1L, False, AnyPropertyType, &type, &format, &length, &after, &data_eroot);

            if (data_root && data_eroot && type == XA_PIXMAP &&
                    *(static_cast<unsigned char *>(data_root)) == *(static_cast<unsigned char *>(data_eroot)))
                XKillClient(display, *(static_cast<unsigned char *>(data_root)));
        }
    }

    atom_root = XInternAtom(display, "_XROOTPMAP_ID", False);
    atom_eroot = XInternAtom(display, "ESETROOT_PMAP_ID", False);

    if (atom_root == None || atom_eroot == None)
        return;

    // setting new background atoms
    XChangeProperty(display, RootWindow(display, screen), atom_root, XA_PIXMAP, 32, PropModeReplace, (unsigned char*)&pixmap, 1);
    XChangeProperty(display, RootWindow(display, screen), atom_eroot, XA_PIXMAP, 32, PropModeReplace, (unsigned char*)&pixmap, 1);

    // setup wallpaper (sync window)
    XKillClient(display, AllTemporary);
    XSetCloseDownMode(display, RetainTemporary);

    XSetWindowBackgroundPixmap(display, RootWindow(display, screen), pixmap);
    XClearWindow(display, RootWindow(display, screen));

    XFlush(display);
    XSync(display, False);
}
