#ifndef FOLDERSDIALOG_H
#define FOLDERSDIALOG_H

#include <QDialog>
#include <QFileDialog>
#include <QSettings>

#include "settings.h"

namespace Ui {
class FoldersDialog;
}

class FoldersDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FoldersDialog(QWidget *parent = nullptr);
    ~FoldersDialog();

private slots:
    void on_bt_add_clicked();
    void on_bt_remove_clicked();

    void on_buttonBox_accepted();

private:
    Ui::FoldersDialog *ui;

    Settings settings;
};

#endif // FOLDERSDIALOG_H
