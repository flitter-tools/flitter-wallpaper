#include "setroot.h"

SetRoot::SetRoot(const QString &file) :
    m_display(QX11Info::display()),
    m_screen(0),
    m_file(file),
    m_direction(0)
{
    m_rec = QGuiApplication::screens()[m_screen]->geometry();
}

SetRoot::SetRoot() :
    m_display(QX11Info::display()),
    m_screen(0),
    m_direction(0)
{
    m_rec = QGuiApplication::screens()[m_screen]->geometry();
}

SetRoot::~SetRoot()
{
}

void SetRoot::set_file(const QString &file)
{
    m_file = file;
}

void SetRoot::set_mode(int mode)
{
    m_mode = mode;
}

void SetRoot::set_colours(const QColor &color1, const QColor &color2)
{
    m_color1 = color1;
    m_color2 = color2;
}

void SetRoot::set_direction(int direction)
{
    m_direction = direction;
}

void SetRoot::set_coordinate(int x, int y)
{
    m_x = x;
    m_y = y;
}

void SetRoot::set_wallpaper()
{
    m_settings.set_direction(m_direction);
    m_settings.set_color(m_color1, m_color2);
    m_settings.set_mode(m_mode);
    m_settings.set_current_wallaper(m_file);

    Imlib imlib(m_display, m_rec);
    imlib.draw(m_file, m_color1, m_color2, m_mode, m_direction);
}

void SetRoot::restore()
{
    Imlib imlib(m_display, m_rec);
    imlib.draw(m_settings.current_wallpaper(),
               m_settings.color().primary,
               m_settings.color().secondary,
               m_settings.mode(),
               m_settings.direction());
}
