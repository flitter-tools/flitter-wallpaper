#ifndef WALLPAPERMODEL_H
#define WALLPAPERMODEL_H

#include <QAbstractListModel>
#include <QVector>
#include <QPixmap>
#include <QtConcurrent/QtConcurrent>

class WallpaperItem;

class WallpaperModel : public QAbstractListModel
{
    Q_OBJECT
    Q_DISABLE_COPY(WallpaperModel)

    public:
        explicit WallpaperModel(QObject *parent = nullptr);
        ~WallpaperModel() override;

        QVariant data(const QModelIndex &index, int role) const override;
        QVariant headerData(int, Qt::Orientation, int role) const override;
        QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
        int columnCount(const QModelIndex &) const override;
        int rowCount(const QModelIndex &) const override;
        Qt::ItemFlags flags(const QModelIndex &index) const override;

        void populate(const QString &folder);

    signals:
        void populate_finished();
        void populate_started();

    private:
        QVector<WallpaperItem*> m_data;

        QString m_folder;
        QFuture<void> m_populate;
};

#endif // WALLPAPERMODEL_H
