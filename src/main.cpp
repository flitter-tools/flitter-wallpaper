#include "mainwindow.h"
#include "setwallpaperdialog.h"
#include "settings.h"

#include <QApplication>
#include <QCommandLineParser>

int main(int argc, char *argv[])
{
    QCoreApplication::setApplicationVersion("1.0");
    QCoreApplication::setApplicationName("Flitter Wallpaper");
    QCoreApplication::setOrganizationName("flitter");

    QApplication app(argc, argv);

    QCommandLineParser parser;
    parser.setApplicationDescription("A simple wallpaper setter based on hsetroot code.");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addOption(QCommandLineOption({"s", "set"}, "Set wallpaper.", "wallpaper path"));
    parser.addOption(QCommandLineOption({"r", "restore"}, "Restore last wallpaper."));
    parser.process(app);

    if (argc == 1)
    {
        MainWindow w;
        w.show();
        return app.exec();
    }
    else
    {
        if (parser.isSet("restore") || parser.isSet("r"))
        {
            SetRoot setroot;
            setroot.restore();
        }
        else if (parser.isSet("set") || parser.isSet("s"))
        {
            QString wallpaper = parser.value("set"); // don't use const here
            SetWallpaperDialog dialog(nullptr, wallpaper);
            dialog.show();
            return app.exec();
        }

        return 0;
    }
}
