#-------------------------------------------------
#
# Project created by QtCreator 2018-06-30T22:28:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets x11extras

TARGET = flitter-wallpaper
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

LIBS += -lX11 -lImlib2

SOURCES += main.cpp\
    colorbutton.cpp \
    folderitem.cpp \
    folderslist.cpp \
    foldersmodel.cpp \
    imlib.cpp \
    mainwindow.cpp\
    settings.cpp \
    setroot.cpp \
    setwallpaperdialog.cpp \
    wallpaperitem.cpp \
    wallpapermodel.cpp \
    wallpaperview.cpp \
    x11.cpp

HEADERS  += mainwindow.h\
    colorbutton.h \
    comboboxdelegate.h \
    folderitem.h \
    folderslist.h \
    foldersmodel.h \
    imlib.h \
    settings.h \
    setroot.h \
    setwallpaperdialog.h \
    wallpaperitem.h \
    wallpapermodel.h \
    wallpaperview.h \
    x11.h

FORMS    += mainwindow.ui \
    setwallpaperdialog.ui

# Make install
bin.path   = /usr/bin
bin.files   = flitter-wallpaper

desktop.path = /usr/share/applications
desktop.files = flitter-wallpaper.desktop

INSTALLS += bin desktop
