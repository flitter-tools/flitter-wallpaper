#ifndef COLORBUTTON_H
#define COLORBUTTON_H

// make button with color filed image

#include <QPushButton>
#include <QStyleOptionButton>
#include <QPainter>
#include <QColorDialog>

class ColorButton : public QPushButton
{
    Q_OBJECT

    public:
        explicit ColorButton(QWidget *parent = nullptr, const QColor &color = Qt::red);

        void set_color(const QColor &color);
        QColor color() const;

    signals:
        void color_changes(const QColor &color);

    protected:
        virtual void paintEvent(QPaintEvent *) override;

    private:
        QColor m_color;

        void open_color_dialog();
};

#endif // COLORBUTTON_H
