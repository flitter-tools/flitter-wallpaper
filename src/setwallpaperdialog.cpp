#include "setwallpaperdialog.h"
#include "ui_setwallpaperdialog.h"

#include "setroot.h"
#include "settings.h"

#include <QPixmap>

SetWallpaperDialog::SetWallpaperDialog(QWidget *parent, const QString &path) :
    QDialog(parent),
    ui(new Ui::SetWallpaperDialog),
    m_file(path)
{
    ui->setupUi(this);

    Settings m_settings;
    ui->ColorButton1->set_color(m_settings.color().primary);
    ui->Mode->setCurrentIndex(m_settings.mode());

    QPixmap pix(path);
    pix = pix.scaled(400, 300, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    ui->Image->setPixmap(pix);
    ui->Image->setAlignment(Qt::AlignCenter);
}

SetWallpaperDialog::~SetWallpaperDialog()
{
    delete ui;
}

void SetWallpaperDialog::on_Mode_currentIndexChanged(int index)
{
    m_mode = index;
}

void SetWallpaperDialog::on_buttonBox_accepted()
{
    SetRoot root;
    root.set_file(m_file);
    root.set_mode(m_mode);
    root.set_colours(ui->ColorButton1->color(), QColor());
    root.set_wallpaper();

    close();
}

void SetWallpaperDialog::on_buttonBox_rejected()
{
    close();
}
