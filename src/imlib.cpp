#include "imlib.h"

Imlib::Imlib(Display *display, const QRect &rec) :
    m_screen(0),
    m_display(display),
    m_rec(rec)
{  
    m_context = static_cast<void**>(imlib_context_new());
    imlib_context_push(m_context);
    imlib_context_set_display(m_display);

    imlib_context_set_display(display);
    m_vis = DefaultVisual(display, m_screen);
    m_colormap = DefaultColormap(display, m_screen);
    m_depth = static_cast<unsigned int>(DefaultDepth(display, m_screen));
}

Imlib::~Imlib()
{
    imlib_context_pop();
    imlib_context_free(m_context);
}

void Imlib::draw(const QString &file, const QColor &color1, const QColor &color2, const int &mode, const int &direction)
{
        Imlib_Image image = imlib_create_image(m_rec.width(), m_rec.height());

        m_pixmap = XCreatePixmap(m_display, RootWindow(m_display, m_screen),
                                   static_cast<unsigned int>(m_rec.width()), static_cast<unsigned int>(m_rec.height()), m_depth);

        // Setup context
        imlib_context_set_visual(m_vis);
        imlib_context_set_colormap(m_colormap);
        imlib_context_set_drawable(m_pixmap);
        imlib_context_set_color_range(imlib_create_color_range());
        imlib_context_set_image(image);

        // Setup colors and gradients
        imlib_context_set_color(color1.red(), color1.green(), color1.blue(), 255);
        if (color2.isValid())
        {
            imlib_context_set_color(color1.red(), color1.green(), color1.blue(), 255);
            imlib_add_color_to_color_range(1);
            imlib_image_fill_color_range_rectangle(0, 0, m_rec.width(), m_rec.height(), 50);
        }
        else
        {
            imlib_image_fill_rectangle(0, 0, m_rec.width(), m_rec.height());
        }

        imlib_context_set_dither(1);
        imlib_context_set_blend(1);

        // Load image to image
        if (!file.isEmpty())
            load_image_impl(mode, image, file);

        // Set direction
        if (direction == Direction::VERTICAL)
            imlib_image_flip_vertical();
        else if (direction == Direction::HORIZONTAL)
            imlib_image_flip_horizontal();
        else if (direction == Direction::DIOGONAL)
            imlib_image_flip_diagonal();

        // Colormap modifier
        if (m_modifier != nullptr)
        {
            imlib_context_set_color_modifier(m_modifier);
            imlib_apply_color_modifier();
            imlib_free_color_modifier();
            m_modifier = nullptr;
        }

        // Render image
        imlib_render_image_on_drawable(0, 0);

        // Free image
        imlib_free_image();
        imlib_free_color_range();

        // Set pixmap to root window
        set_root_atoms(m_display, m_pixmap);
}

void Imlib::load_image_impl(int mode, Imlib_Image rootimg, const QString &file)
{
    int imgW, imgH;
    Imlib_Image buffer = imlib_load_image(file.toLatin1());

    if (!buffer)
        return;

    imlib_context_set_image(buffer);
    imgW = imlib_image_get_width();
    imgH = imlib_image_get_height();

    imlib_context_set_image(rootimg);

    if (mode == Mode::FILL)
    {
        imlib_blend_image_onto_image(buffer, 0, 0, 0, imgW, imgH, 0, 0, m_rec.width(), m_rec.height());
    }
    else if ((mode == Mode::FULL) || (mode == Mode::XTEND) || (mode == Mode::COVER))
    {
        double aspect = static_cast<double>(m_rec.width()) / imgW;
        if ((static_cast<int>(imgH * aspect) > m_rec.height()) != (mode == Mode::COVER))
            aspect = static_cast<double>(m_rec.height()) / static_cast<double>(imgH);

        int top = (m_rec.height() - static_cast<int>(imgH * aspect)) / 2;
        int left = (m_rec.width() - static_cast<int>(imgW * aspect)) / 2;

        imlib_blend_image_onto_image(buffer,
                                                             0, 0, 0,
                                                             imgW, imgH,
                                                             0 + left, 0 + top,
                                                             static_cast<int>(imgW * aspect),
                                                             static_cast<int>(imgH * aspect));

        if (mode == Mode::XTEND)
        {
            int w;

            if (left > 0)
            {
                int right = left - 1 + (int) (imgW * aspect);
                /* check only the right border - left is int divided so the right border is larger */
                for (w = 1; right + w < m_rec.width(); w <<= 1)
                {
                    imlib_image_copy_rect(left + 1 - w, 0, w, m_rec.height(), left + 1 - w - w, 0);
                    imlib_image_copy_rect(right, 0, w, m_rec.height(), right + w, 0);
                }
            }

            if (top > 0)
            {
                int bottom = top - 1 + (int) (imgH * aspect);
                for (w = 1; (bottom + w < m_rec.height()); w <<= 1)
                {
                    imlib_image_copy_rect(0, top + 1 - w, m_rec.width(), w, 0, top + 1 - w - w);
                    imlib_image_copy_rect(0, bottom, m_rec.width(), w, 0, bottom + w);
                }
            }
        }
    }
    else // Center || Tile
    {
        int left = (m_rec.width() - imgW) / 2;
        int top = (m_rec.height() - imgH) / 2;

        if (mode == Mode::TILE)
        {
            int x, y;
            for (; left > 0; left -= imgW);
            for (; top > 0; top -= imgH);

            for (x = left; x < m_rec.width(); x += imgW)
            {
                for (y = top; y < m_rec.height(); y += imgH)
                {
                    imlib_blend_image_onto_image(buffer, 0, 0, 0, imgW, imgH, x, y, imgW, imgH);
                }
            }
        }
        else
        {
            imlib_blend_image_onto_image(buffer, 0, 0, 0, imgW, imgH, 0 + left, 0 + top, imgW, imgH);
        }
    }

    imlib_context_set_image(buffer);
    imlib_free_image();

    imlib_context_set_image(rootimg);
}
