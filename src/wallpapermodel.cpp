#include "wallpapermodel.h"
#include "wallpaperitem.h"

#include <QDir>
#include <QFileInfo>
#include <QDebug>

WallpaperModel::WallpaperModel(QObject *parent) :
    QAbstractListModel(parent)
{
}

WallpaperModel::~WallpaperModel()
{
    m_populate.cancel();
    m_populate.waitForFinished();
    qDeleteAll(m_data);
}

void WallpaperModel::populate(const QString &folder)
{
    m_folder = folder;

    emit populate_started();

    m_populate = QtConcurrent::run([this](){
        QDir dir(m_folder);
        dir.setNameFilters(QStringList() << "*.png" << "*.jpg" << "*.jpeg");
        QFileInfoList list = dir.entryInfoList();

        beginResetModel();
        qDeleteAll(m_data);
        m_data.clear();

        for (const auto &item : list)
            m_data.push_back(new WallpaperItem(item.filePath()));

        endResetModel();

        emit populate_finished();
    });
}

QVariant WallpaperModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    const auto *item = static_cast<WallpaperItem*>(index.internalPointer());

    if (role == Qt::DecorationRole)
        return item->image();

    return QVariant();
}

QVariant WallpaperModel::headerData(int, Qt::Orientation, int) const
{
    return QVariant();
}

QModelIndex WallpaperModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    return createIndex(row, column, m_data.at(row));
}

int WallpaperModel::columnCount(const QModelIndex &) const
{
    return 1;
}

int WallpaperModel::rowCount(const QModelIndex &) const
{
    return m_data.length();
}

Qt::ItemFlags WallpaperModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}
