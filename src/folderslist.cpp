#include "folderslist.h"

#include "folderitem.h"
#include "foldersmodel.h"
#include "comboboxdelegate.h"

#include <QModelIndex>

FoldersList::FoldersList(QWidget *parent) :
    QComboBox(parent)
{
    auto *model = new FoldersModel(this);
    model->populate();
    setModel(model);

    setItemDelegate(new ComboBoxDelegate);
}

FoldersModel *FoldersList::model() const
{
    return qobject_cast<FoldersModel *>(QComboBox::model());
}

FolderItem *FoldersList::current_item() const
{
    const QModelIndex &index = model()->index(currentIndex(), 1);
    return static_cast<FolderItem*>(index.internalPointer());
}

void FoldersList::setModel(QAbstractItemModel *model)
{
    QComboBox::setModel(model);
}
