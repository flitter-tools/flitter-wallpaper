#ifndef SETWALLPAPERDIALOG_H
#define SETWALLPAPERDIALOG_H

#include <QDialog>

namespace Ui {
class SetWallpaperDialog;
}

class SetWallpaperDialog : public QDialog
{
    Q_OBJECT

    public:
        explicit SetWallpaperDialog(QWidget *parent = nullptr, const QString &path = "");
        ~SetWallpaperDialog();

    private slots:
        void on_Mode_currentIndexChanged(int index);
        void on_buttonBox_accepted();
        void on_buttonBox_rejected();

    private:
        Ui::SetWallpaperDialog *ui;
        int m_mode;
        QString m_file;
};

#endif // SETWALLPAPERDIALOG_H
