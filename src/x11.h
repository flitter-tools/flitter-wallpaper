#ifndef X11_H
#define X11_H

#include <X11/Xlib.h>
#include <X11/Xatom.h>

void set_root_atoms(Display *display, Pixmap pixmap);

#endif // X11_H
