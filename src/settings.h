#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>
#include <QStringList>
#include <QString>
#include <QDir>
#include <QColor>

enum Mode
{
    WALLPAPER = 0,
    SOLIDCOLOR,
    GRADIENT
};

enum Style
{
    CENTER = 0,
    COVER,
    TILE,
    FULL,
    FILL
};

class Settings : private QSettings
{
    public:
        Settings();
        ~Settings(){}

        struct Color
        {
            QColor primary;
            QColor secondary;
        };

        QStringList folders();
        void set_folders(QStringList folders);
        void remove_folder(int row);

        QString current_wallpaper() const;
        QString current_folder() const;
        int mode() const;
        int style() const;
        int direction() const;
        Color color() const;

        void set_current_wallaper(const QString &wall);
        void set_current_folder(const QString &folder);
        void set_direction(int direction);
        void set_mode(int mode);
        void set_style(int style);
        void set_color(const QColor &color_primary, const QColor &color_secondary);
};

#endif // SETTINGS_H
