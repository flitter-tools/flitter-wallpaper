#include "settings.h"

#include <QColor>
#include <QStandardPaths>
#include <QDebug>

Settings::Settings() :
    QSettings(
        QStandardPaths::standardLocations(QStandardPaths::ConfigLocation).first() + "/flitter/wallpaper.conf",
        QSettings::IniFormat)
{
    setIniCodec("UTF-8");
}

QStringList Settings::folders()
{ 
    QStringList all;

    beginGroup("folder");
    for (const QString &key : allKeys())
        all.push_back(value(key).toString());
    endGroup();

    return all;
}

void Settings::set_folders(QStringList folders)
{
    for (auto i = 0; i < folders.count(); ++i)
        setValue("folder/folder" + QString::number(i), folders.at(i));
}

void Settings::remove_folder(int row)
{
    remove("folder/" + QString::number(row) + "folder");
}

void Settings::set_style(int style)
{
    setValue("style", style);
}

int Settings::style() const
{
    return value("style").toInt();
}

void Settings::set_mode(int mode)
{
    setValue("mode", mode);
}

int Settings::mode() const
{
    return value("mode").toInt();
}

void Settings::set_direction(int direction)
{
    setValue("direction", direction);
}

int Settings::direction() const
{
    return value("direction").toInt();
}

QString Settings::current_wallpaper() const
{
    return value("wallpaper").toString();
}

void Settings::set_current_wallaper(const QString &wall)
{
    setValue("wallpaper", wall);
}

QString Settings::current_folder() const
{
    return value("currentFolder").toString();
}

void Settings::set_current_folder(const QString &folder)
{
    setValue("currentFolder", folder);
}

Settings::Color Settings::color() const
{
    Settings::Color ret;
    ret.primary = value("colorPrimary").toString();
    ret.secondary = value("colorSecondary").toString();

    return ret;
}

void Settings::set_color(const QColor &color_primary, const QColor &color_secondary)
{
    setValue("colorPrimary", color_primary.name());
    setValue("colorSecondary", color_secondary.name());
}
