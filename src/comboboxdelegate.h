#ifndef COMBOBOXDELEGATE_H
#define COMBOBOXDELEGATE_H

#include <QItemDelegate>
#include <QPainter>

class ComboBoxDelegate : public QItemDelegate
{
    Q_OBJECT
    public:
        explicit ComboBoxDelegate(QObject *parent = nullptr) : QItemDelegate(parent){}

    protected:
        void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
        {
            if(index.data(Qt::AccessibleDescriptionRole).toString() == QLatin1String("separator"))
            {
                painter->setPen(Qt::gray);
                painter->drawLine(option.rect.left(), option.rect.center().y(), option.rect.right(), option.rect.center().y());
            }
            else
                QItemDelegate::paint(painter, option, index);
        }

        QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
        {
            QString type = index.data(Qt::AccessibleDescriptionRole).toString();
            if(type == QLatin1String("separator"))
                return QSize(0, 5);
            return QItemDelegate::sizeHint( option, index );
        }
};

#endif // COMBOBOXDELEGATE_H
