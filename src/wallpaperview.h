#ifndef WALLPAPERVIEW_H
#define WALLPAPERVIEW_H

#include <QListView>

class WallpaperItem;
class WallpaperModel;

class WallpaperView : public QListView
{
    Q_OBJECT
    Q_DISABLE_COPY(WallpaperView)

    public:
        explicit WallpaperView(QWidget *parent = nullptr);

        WallpaperModel *model() const;
        WallpaperItem *current_item() const;

    signals:
        void current_item_changed(WallpaperItem *item);

    private:
        void setModel(QAbstractItemModel *model) override;
};

#endif // WALLPAPERVIEW_H
