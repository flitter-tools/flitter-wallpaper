#include "colorbutton.h"

ColorButton::ColorButton(QWidget *parent, const QColor &color) :
    QPushButton(parent),
    m_color(color)
{
    setMaximumSize(46, 27);

    connect(this, &QPushButton::clicked, this, &ColorButton::open_color_dialog);
}

void ColorButton::set_color(const QColor &color)
{
    m_color = color;
    update();
}

QColor ColorButton::color() const
{
    return m_color;
}

void ColorButton::paintEvent(QPaintEvent *)
{
    int w = width()-10;
    int h = height()-10;

    QStyleOptionButton option;
    option.initFrom(this);
    option.state = isDown() ? QStyle::State_Sunken : QStyle::State_Raised;
    option.features |= QStyleOptionButton::DefaultButton;

    QPainter painter(this);

    style()->drawControl(QStyle::CE_PushButton, &option, &painter, this);

    if (this->isEnabled())
    {
        painter.setPen(Qt::black);
        painter.setBrush(m_color);
    }
    else
    {
        QColor disabled = Qt::gray;
        disabled.setAlpha(10);
        painter.setBrush(disabled);
    }

    painter.drawRect(QRect(width()/2-w/2, height()/2-h/2, w, h));
}

void ColorButton::open_color_dialog()
{
    QColorDialog dialog;
    if (dialog.exec() == QColorDialog::Accepted)
    {
        m_color = dialog.selectedColor();
        emit color_changes(m_color);
    }
}
