#include "foldersmodel.h"
#include "folderitem.h"

#include <QStandardPaths>
#include <QDebug>

FoldersModel::FoldersModel(QObject *parent) :
    QAbstractListModel(parent)
{
}

FoldersModel::~FoldersModel()
{
    qDeleteAll(m_data);
}

void FoldersModel::populate()
{
    beginResetModel();
    m_data.clear();

    QString pictures = QStandardPaths::standardLocations(QStandardPaths::PicturesLocation).first();
    m_data.push_back(new FolderItem(pictures, "folder-pictures"));

    for (const auto &item : m_settings.folders())
        m_data.push_back(new FolderItem(item, "folder"));

    m_data.push_back(new FolderItem("separator", ""));
    m_data.push_back(new FolderItem("Search...", "search"));

    endResetModel();
}

QVariant FoldersModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    const auto *item = static_cast<FolderItem*>(index.internalPointer());

    if (role == Qt::DecorationRole)
        return item->icon();
    else if (role == Qt::DisplayRole)
        return item->description();
    else if (role == Qt::AccessibleDescriptionRole)
        return item->description();

    return QVariant();
}

QModelIndex FoldersModel::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    //if (!hasIndex(row, column, parent))
    //    return QModelIndex();

    return createIndex(row, column, m_data.at(row));
}

int FoldersModel::columnCount(const QModelIndex &) const
{
    return 1;
}

int FoldersModel::rowCount(const QModelIndex &) const
{
    return m_data.length();
}
