#include "wallpaperitem.h"

#include <QDebug>

WallpaperItem::WallpaperItem(const QString &image_path) :
    m_image_path(image_path)
{
    m_image.load(image_path);
    m_image = m_image.scaled(200, 100, Qt::KeepAspectRatio, Qt::SmoothTransformation);
}

WallpaperItem::~WallpaperItem()
{
}

QPixmap WallpaperItem::image() const
{
    return m_image;
}

QString WallpaperItem::image_path() const
{
    return m_image_path;
}
