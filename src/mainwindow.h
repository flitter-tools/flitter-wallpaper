#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "setroot.h"

#include <QMainWindow>
#include <QListWidgetItem>
#include <QPixmap>
#include <QIcon>
#include <QSettings>
#include <QDir>
#include <QString>
#include <QtConcurrent/qtconcurrentrun.h>

#include "comboboxdelegate.h"
#include "wallpapermodel.h"
#include "wallpaperitem.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

    private slots:
        void on_mode_changed(int index);
        void on_current_item_changed(WallpaperItem *item);
        void on_color_changed(const QColor &color);
        void on_populate_finished();
        void on_populate_started();
        void on_DialogExit_clicked();
        void on_FoldersCombo_currentIndexChanged(int index);

private:
        void load_settings();
        void load_actions();

        WallpaperItem *current_item() const;
        QString current_image() const;

        SetRoot setroot;

        Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
