#include "folderitem.h"

#include <QFileInfo>
#include <QDebug>

FolderItem::FolderItem(const QString &path, const QString &icon) :
    m_path(path),
    m_icon(QIcon::fromTheme(icon))
{
    if (!path.isEmpty() && path != "separator")
        m_description = QFileInfo(m_path).baseName();
    else
        m_description = "separator";
}

FolderItem::~FolderItem()
{
}

QString FolderItem::path() const
{
    return m_path;
}

QIcon FolderItem::icon() const
{
    return m_icon;
}

QString FolderItem::description() const
{
    return m_description;
}
