#include "foldersdialog.h"
#include "ui_foldersdialog.h"

FoldersDialog::FoldersDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FoldersDialog)
{
    ui->setupUi(this);

    for (QString folder : settings.getFolders())
        ui->lw_folders->addItem(folder);
}

FoldersDialog::~FoldersDialog()
{
    delete ui;
}

void FoldersDialog::on_bt_add_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                        QDir::homePath(),
                                                        QFileDialog::ShowDirsOnly
                                                        | QFileDialog::DontResolveSymlinks);

    if (!dir.isEmpty())
        ui->lw_folders->addItem(dir);
}

void FoldersDialog::on_bt_remove_clicked()
{
    QListWidgetItem *item = ui->lw_folders->currentItem();
    if (item != nullptr)
    {
        settings.removeFolder(ui->lw_folders->currentRow());
        delete item;
    }
}

void FoldersDialog::on_buttonBox_accepted()
{
    QStringList all;
    for (auto i = 0; i < ui->lw_folders->count(); ++i)
        all.push_back(ui->lw_folders->item(i)->text());
    settings.setFolders(all);

    accept();
}
