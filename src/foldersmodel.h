#ifndef FOLDERSMODEL_H
#define FOLDERSMODEL_H

#include <QAbstractListModel>
#include <QVector>
#include <QPixmap>

#include "settings.h"

class FolderItem;

class FoldersModel : public QAbstractListModel
{
    Q_OBJECT
    Q_DISABLE_COPY(FoldersModel)

    public:
        explicit FoldersModel(QObject *parent = nullptr);
        ~FoldersModel() override;

        QVariant data(const QModelIndex &index, int role) const override;
        QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
        int columnCount(const QModelIndex &) const override;
        int rowCount(const QModelIndex &) const override;

        void populate();

    private:
        QVector<FolderItem*> m_data;

        Settings m_settings;
};

#endif // FOLDERSMODEL_H
