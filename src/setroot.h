#ifndef SETROOT_H
#define SETROOT_H

#include <QString>
#include <QRect>
#include <QColor>
#include <QScreen>
#include <QApplication>
#include <QX11Info>

#include "imlib.h"
#include "settings.h"

class SetRoot
{
    public:
        SetRoot(const QString &file);
        SetRoot();
        ~SetRoot();

        void set_file(const QString &file);
        void set_wallpaper();
        void set_mode(int mode);
        void set_direction(int direction);
        void set_colours(const QColor &color1, const QColor &color2);
        void set_coordinate(int x, int y);

        void restore();

    private:
        Display *m_display;
        int m_screen;
        QRect m_rec;
        Settings m_settings;

        QString m_file;
        int m_mode;
        int m_direction;
        QColor m_color1;
        QColor m_color2;
        int m_x;
        int m_y;
};

#endif // SETROOT_H
