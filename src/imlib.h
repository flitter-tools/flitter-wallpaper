#ifndef IMLIB_H
#define IMLIB_H

#include <QColor>
#include <QString>
#include <QRect>

#include "x11.h"
#include <Imlib2.h>

// This is define by Xlib header, but interferes Qt one
#undef Bool
#undef Status
#undef True
#undef False
#undef None
#undef Unsorted

class Imlib
{
    public:
        Imlib(Display *display, const QRect &rec);
        ~Imlib();

        enum Direction
        {
            VERTICAL = 1,
            HORIZONTAL,
            DIOGONAL
        };

        enum Mode
        {
            CENTER = 0,
            COVER,
            TILE,
            FULL,
            FILL,
            XTEND,
        };

        void draw(const QString &file, const QColor &color1, const QColor &color2, const int &mode, const int &direction);

    private:
        void load_image_impl(int mode, Imlib_Image rootimg, const QString &file);

        Imlib_Context *m_context;
        Visual *m_vis;
        Colormap m_colormap;
        unsigned int m_depth;
        Pixmap m_pixmap;
        Imlib_Color_Modifier m_modifier = nullptr;
        int m_screen;
        Display *m_display;
        QRect m_rec;
};

#endif // IMLIB_H
