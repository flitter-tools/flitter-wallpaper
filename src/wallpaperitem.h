#ifndef WALLPAPERITEM_H
#define WALLPAPERITEM_H

#include <QString>
#include <QPixmap>

class WallpaperItem
{
    Q_DISABLE_COPY(WallpaperItem)

    public:
        WallpaperItem(const QString &image_path);
        ~WallpaperItem();

        QPixmap image() const;
        QString image_path() const;

    private:
        QString m_image_path;
        QPixmap m_image;
};

#endif // WALLPAPERITEM_H
