#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "colorbutton.h"
#include "settings.h"
#include "foldersmodel.h"
#include "folderitem.h"

#include <QShortcut>
#include <QStandardPaths>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    auto *model = ui->WallpapersList->model();
    model->populate(QStandardPaths::standardLocations(QStandardPaths::PicturesLocation).first());

    ui->icon->setPixmap(QIcon::fromTheme("preferences-desktop-wallpaper").pixmap(45));

    load_settings();
    load_actions();

    connect(ui->WallpapersList->model(), &WallpaperModel::populate_started, this, &MainWindow::on_populate_started);
    connect(ui->WallpapersList->model(), &WallpaperModel::populate_finished, this, &MainWindow::on_populate_finished);
    connect(ui->ColourPrimary, &ColorButton::color_changes, this, &MainWindow::on_color_changed);
    connect(ui->WallpapersList, &WallpaperView::current_item_changed, this, &MainWindow::on_current_item_changed);
    connect(ui->WallpaperMode, qOverload<int>(&QComboBox::currentIndexChanged), this, &MainWindow::on_mode_changed);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_populate_started()
{
    ui->WallpapersList->update();

    ui->SearchField->setEnabled(false);
    ui->WallpapersList->setEnabled(false);
    ui->WallpaperMode->setEnabled(false);
    ui->WallpaperStyle->setEnabled(false);
    ui->FoldersCombo->setEnabled(false);
    ui->ColourPrimary->setEnabled(false);
    ui->changeBackgroundsRandom->setEnabled(false);
}

void MainWindow::on_populate_finished()
{
    ui->SearchField->setEnabled(true);
    ui->WallpapersList->setEnabled(true);
    ui->WallpaperMode->setEnabled(true);
    ui->WallpaperStyle->setEnabled(true);
    ui->FoldersCombo->setEnabled(true);
    ui->ColourPrimary->setEnabled(true);
    ui->changeBackgroundsRandom->setEnabled(true);
}

QString MainWindow::current_image() const
{
    return ui->WallpapersList->current_item()->image_path();
}

void MainWindow::on_mode_changed(int index)
{
    SetRoot setroot(current_image());
    setroot.set_mode(index);
    setroot.set_colours(ui->ColourPrimary->color(), QColor());
    setroot.set_wallpaper();
}

void MainWindow::load_actions()
{
    QShortcut *searchAction = new QShortcut(this);
    searchAction->setKey(QKeySequence("Ctrl+F"));
    connect(searchAction, &QShortcut::activated, [&](){
        ui->SearchField->setFocus();
    });

    QShortcut *quitAction = new QShortcut(this);
    quitAction->setKey(QKeySequence("Ctrl+Q"));
    connect(quitAction, &QShortcut::activated, qApp, &QApplication::quit);
}

void MainWindow::load_settings()
{
    Settings settings;

    ui->ColourPrimary->set_color(settings.color().primary);
    ui->ColourSecondary->set_color(settings.color().secondary);
    ui->WallpaperMode->setCurrentIndex(settings.style());
    ui->WallpaperStyle->setCurrentIndex(settings.mode());
}

void MainWindow::on_current_item_changed(WallpaperItem *item)
{
    const QString &file = item->image_path();

    SetRoot setroot(file);
    setroot.set_mode(ui->WallpaperMode->currentIndex());
    setroot.set_colours(ui->ColourPrimary->color(), QColor());
    setroot.set_wallpaper();
}

void MainWindow::on_color_changed(const QColor &color)
{
    setroot.set_file(current_image());
    setroot.set_mode(ui->WallpaperMode->currentIndex());
    setroot.set_colours(color, QColor());
    setroot.set_wallpaper();
}

void MainWindow::on_DialogExit_clicked()
{
     qApp->exit();
}

void MainWindow::on_FoldersCombo_currentIndexChanged(int index)
{
    Settings settings;

    if (index == ui->FoldersCombo->count() - 1)
    {
        ui->FoldersCombo->setCurrentIndex(0);

        QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                        "/home",
                                                        QFileDialog::ShowDirsOnly
                                                        | QFileDialog::DontResolveSymlinks);

        if (!dir.isEmpty())
        {
            settings.set_folders(settings.folders() << dir);

            auto *model = static_cast<FoldersModel*>(ui->FoldersCombo->model());
            model->populate();

            ui->FoldersCombo->setCurrentIndex(ui->FoldersCombo->count()-3);
        }
    }
    else
    {
        auto *item = static_cast<FolderItem*>(ui->FoldersCombo->current_item());
        auto *model = static_cast<WallpaperModel*>(ui->WallpapersList->model());
        model->populate(item->path());
    }
}
