#ifndef FOLDERSLIST_H
#define FOLDERSLIST_H

#include <QComboBox>

class FolderItem;
class FoldersModel;

class FoldersList : public QComboBox
{
    Q_OBJECT
    Q_DISABLE_COPY(FoldersList)

    public:
        explicit FoldersList(QWidget *parent = nullptr);

        FoldersModel *model() const;
        FolderItem *current_item() const;

    signals:
        void current_item_changed(FolderItem *item);

    private:
        void setModel(QAbstractItemModel *model);
};

#endif // FOLDERSLIST_H
