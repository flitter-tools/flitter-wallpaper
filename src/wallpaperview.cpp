#include "wallpaperview.h"

#include "wallpaperitem.h"
#include "wallpapermodel.h"

#include <QDebug>

WallpaperView::WallpaperView(QWidget *parent) :
    QListView(parent)
{
    setFrameShape(QFrame::Box);
    setSelectionBehavior(QAbstractItemView::SelectRows);
    setIconSize(QSize(200, 100));
    setMovement(Movement::Snap);
    setResizeMode(ResizeMode::Adjust);
    setGridSize(QSize(190, 190));
    setViewMode(ViewMode::IconMode);
    setItemAlignment(Qt::AlignVCenter);

    setModel(new WallpaperModel(this));

    connect(selectionModel(), &QItemSelectionModel::currentChanged, [this](const QModelIndex &current) {
        auto *item = static_cast<WallpaperItem*>(current.internalPointer());
        emit current_item_changed(item);
    });
}

WallpaperModel *WallpaperView::model() const
{
    return qobject_cast<WallpaperModel *>(QListView::model());
}

WallpaperItem *WallpaperView::current_item() const
{
    return static_cast<WallpaperItem*>(currentIndex().internalPointer());
}

void WallpaperView::setModel(QAbstractItemModel *model)
{
    QListView::setModel(model);
}
